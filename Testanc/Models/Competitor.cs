﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Testanc.Models
{
    public class Competitor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Level { get; set; }
        public string Nationality { get; set; }
    
    }
}
