﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Testanc.Models
{
    public class CompetitorDbContext : DbContext

    {
        public CompetitorDbContext(DbContextOptions<CompetitorDbContext>options) : base(options) { }
        
        public DbSet<Competitor> Competitors { get; set; }

    }

}
