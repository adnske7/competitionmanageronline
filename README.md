# Competition Manager
Program that allows you to create competitors.

## Getting Started
Open the folder in Visual studio, and run the program.cs file there. The program should open the page in your browser, then just click on competitor to add, see details or delete.


## Author
* **Ådne Skeie** - [adnske7](https://gitlab.com/adnske7)
